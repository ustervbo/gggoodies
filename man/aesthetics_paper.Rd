% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/aesthetics.R
\name{aesthetics_paper}
\alias{aesthetics_paper}
\alias{aesthetics_powerpoint}
\title{gggoodies aesthetics for papers and presentations}
\usage{
aesthetics_paper()

aesthetics_powerpoint()
}
\value{
A list of named lists
}
\description{
Pretty and useful aesthetics to be used in paprs or presentations.
}
\examples{
aesthetics_paper()
aesthetics_powerpoint()

}
