
# gggoodies<img src="man/figures/logo.png" width="120px" align="right" />

[![pipeline
status](https://gitlab.com/ustervbo/gggoodies//badges/master/pipeline.svg)](https://gitlab.com/ustervbo/gggoodies//commits/master)
[![coverage
report](https://gitlab.com/ustervbo/gggoodies//badges/master/coverage.svg)](https://gitlab.com/ustervbo/gggoodies//commits/master)
[![lifecycle](https://img.shields.io/badge/lifecycle-experimental-orange.svg)](https://www.tidyverse.org/lifecycle/#experimental)

The default `ggplot` implementation does not allow the use of different
aesthetics for the same geom. It is also not possible to set a theme to
a ggplot without overwriting manually set elements. This package solves
these two problems by manipulating the ggplot objects directly.

## Installation

You can install the development version from GitLab with:

``` r
# install.packages("remotes")
remotes::install_gitlab("ustervbo/gggoodies")
```

## Example

``` r
library(ggplot2)
library(gggoodies)
```

### Theme modifications

Create a plot with some changes to the theme:

``` r
p <- ggplot(iris) +
  aes(x = Species, y = Petal.Width) +
  geom_point() +
  theme(axis.title.x = element_text(colour = "red", size = 18),
        axis.text.y = element_text(colour ="blue", angle = 90, size = 12))

p
```

<img src="man/figures/README-apply-theme-setup-1.png" width="60%" style="display: block; margin: auto;" />

Using `p + theme_dark()` results in reset of all manual additions to the
values of the new theme and all our hard work is sadly
lost:

<img src="man/figures/README-apply-theme-problem-1.png" width="60%" style="display: block; margin: auto;" />

The `apply_theme()` solves this problem, by only changeing the theme
elements in the plot that have not been
set:

``` r
apply_theme(p, .t = theme_bw, force = FALSE)
```

<img src="man/figures/README-apply-theme-solution-1.png" width="60%" style="display: block; margin: auto;" />

### Aesthetics modifications

The default settings for the geom can be set using
`update_geom_defaults()` which takes the name of the geom (`point` for
instance) and a named list with the new settings (`list(size = 5, alpha
= 0.5)`). The problem is that it changes all plots:

``` r
p <- ggplot(iris) +
  aes(x = Petal.Length, y = Petal.Width) +
  geom_point()
p
```

<img src="man/figures/README-apply-aesthetics-1.png" width="60%" style="display: block; margin: auto;" />

``` r
update_geom_defaults("point", list(size = 5, alpha = 0.5))

p
```

<img src="man/figures/README-apply-aesthetics-problem-1.png" width="60%" style="display: block; margin: auto;" />

``` r
ggplot(iris) +
  aes(x = Sepal.Length, y = Sepal.Width, colour = Species) +
  geom_point()
```

<img src="man/figures/README-apply-aesthetics-problem2-1.png" width="60%" style="display: block; margin: auto;" />

The `apply_aesthetics()` solves this problem, by only changeing the
aesthetics elements in the
plot:

``` r
apply_aesthetics(p, aesthetics_paper)
```

<img src="man/figures/README-apply-aesthetics-solution-1.png" width="60%" style="display: block; margin: auto;" />

``` r
ggplot(iris) +
  aes(x = Sepal.Length, y = Sepal.Width, colour = Species) +
  geom_point()
```

<img src="man/figures/README-apply-aesthetics-solution2-1.png" width="60%" style="display: block; margin: auto;" />

### Set default palette

I like the viridis colour palette more than I like the default ggplot2
one (which I used to like, so nothing lasts forever). The defaults chan
be changed with:

``` r
# Set viridis in reverse direction
set_viridis_default(direction = -1)
```

``` r
ggplot(iris) +
  aes(x = Sepal.Length, y = Sepal.Width, colour = Species) +
  geom_point()
```

<img src="man/figures/README-viridis_default_colour_d-1.png" width="60%" style="display: block; margin: auto;" />

``` r
ggplot(iris) +
  aes(x = Sepal.Length, y = Sepal.Width, colour = Petal.Width) +
  geom_point()
```

<img src="man/figures/README-viridis_default_colour_c-1.png" width="60%" style="display: block; margin: auto;" />

``` r
ggplot(iris) +
  aes(x = Species, y = Sepal.Width, fill = Species) +
  geom_col()
```

<img src="man/figures/README-viridis_default_fill_d-1.png" width="60%" style="display: block; margin: auto;" />

This is just for demonstration purposes…

``` r
ggplot(iris) +
  aes(x = Species, y = Sepal.Width, fill = Petal.Width) +
  geom_col()
```

<img src="man/figures/README-viridis_default_fill_c-1.png" width="60%" style="display: block; margin: auto;" />
