context("test-default-palette")

test_plot_cd <- function(){
  ggplot(iris) +
    aes(x = Sepal.Length, y = Sepal.Width, colour = Species) +
    geom_point()
}

test_plot_cc <- function(){
  ggplot(iris) +
    aes(x = Sepal.Length, y = Sepal.Width, colour = Petal.Width) +
    geom_point()
}

test_plot_fd <- function(){
  ggplot(iris) +
    aes(x = Species, y = Sepal.Width, fill = Species) +
    geom_col()
}

test_plot_fc <- function(){
  ggplot(iris) +
    aes(x = Species, y = Sepal.Width, fill = Petal.Width) +
    geom_col()
}


test_that("palette can be set", {
  library(ggplot2)

  expect_silent(set_viridis_default())
  expect_silent(set_viridis_discrete())
  expect_silent(set_viridis_continuous())
  expect_silent(set_OkabeIto_default())
})

test_that("palette does not break plots", {
  library(ggplot2)

  plot_lst <- list(test_plot_cd(), test_plot_cc(), test_plot_fd(), test_plot_fc())

  lapply(plot_lst, function(x){
    expect_is(x, "gg")
    expect_is(x, "ggplot")
  })
})
